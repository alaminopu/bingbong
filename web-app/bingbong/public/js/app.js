var app = angular.module('iha', ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('home', {
      url: '/home',
      templateUrl: 'templates/home.html',
      controller: 'ToggleCtrl'
  })

  .state('login', {
      url: '/',
      templateUrl: 'templates/login.html',
      controller: 'LoginCtrl'
  })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/');

});

app.factory('httpService', function($http){
	return {
		list: function(){
			return $http.get('http://bingbong.website/api/switches/list');
		},
		save: function(data){
			return $http.get('http://bingbong.website/api/switches/save', {params: data});
		}
	}
});

app.service('LoginService', function($q) {
  return {
    loginUser: function(name, pw) {
        var deferred = $q.defer();
        var promise = deferred.promise;

        if (name == 'user' && pw == 'secret') {
            deferred.resolve('Welcome ' + name + '!');
        } else {
            deferred.reject('Wrong credentials.');
        }
        promise.success = function(fn) {
            promise.then(fn);
            return promise;
        }
        promise.error = function(fn) {
            promise.then(null, fn);
            return promise;
        }
        return promise;
    }
  }
});

app.controller('LoginCtrl', function($scope, LoginService, $state){
	$scope.data = {};
	$scope.showModal = false;
 
	$scope.login = function(){
      LoginService.loginUser($scope.data.username, $scope.data.password).success(function(data) {
            $state.go('home');
        }).error(function(data) {
            $scope.showModal = !$scope.showModal;
        });
	}

});

app.controller('ToggleCtrl', function($scope, httpService){
	$scope.switches = [
		{
			text: 'Light_1',
			checked: false
		},
		{
			text: 'Light_2',
			checked: true
		},
		{
			text: 'Fan',
			checked: false
		}
	];

	$scope.refresh = function(){
		getStatus();
	}

	getStatus();

	function getStatus(){
		httpService.list().then(
			function(result){
				$scope.switches = result.data;
			},
			function(err){
				console.log(err);
			}
		);
	}

	$scope.update = function(model){
		if(model.checked == false){
			model.checked = true;
		}else{
			model.checked = false;
		}
		var data = {text:model.text, checked: model.checked};
		console.log(data);
		httpService.save(data).then(
			function(data){
				console.log(data);
			}, 
			function(err){
				console.log(err);
			}
		);
	}

	$scope.logout = function(){
        $state.go('login');
    }

})

app.directive('modal', function () {
    return {
      template: '<div class="modal fade">' + 
          '<div class="modal-dialog">' + 
            '<div class="modal-content">' + 
              '<div class="modal-header">' + 
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
                '<h4 class="modal-title">{{ title }}</h4>' + 
              '</div>' + 
              '<div class="modal-body" ng-transclude></div>' + 
            '</div>' + 
          '</div>' + 
        '</div>',
      restrict: 'E',
      transclude: true,
      replace:true,
      scope:true,
      link: function postLink(scope, element, attrs) {
        scope.title = attrs.title;

        scope.$watch(attrs.visible, function(value){
          if(value == true)
            $(element).modal('show');
          else
            $(element).modal('hide');
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  });