<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

use Illuminate\Http\Request;

use DB;

class Controller extends BaseController
{
    /**
    *  request for switch status
    */
    public function allSwitchStatus(){
    	$results = DB::select('select * from iha');
    	$switches = array();

    	if(is_array($results)){
    		foreach($results as $row){
    			array_push($switches, array('text'=>$row->name, 'checked'=>$row->status?true:false));
    		}
    	}

    	return response()
    	->json($switches)
    	->header('Access-Control-Allow-Origin', '*');
    }

    /**
    *	Update Switch status
    */

    public function updateSwitchStatus(Request $request){
    	$input =$request->all();

    	foreach($input as $key=>$value){
    		$data = $key;
    	}
    	$result = json_decode($data);

    	$affected = DB::update('update iha set name = ? , status = ? where name = ?', [$result->text, $result->checked, $result->text]);

    	return response()
    	->json($affected)
    	->header('Access-Control-Allow-Origin', '*');
    }
}
