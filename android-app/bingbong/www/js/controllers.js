angular.module('starter.controllers', [])


// httpService factory

.factory('httpService', function($http){
    return {
      list: function(){
        return $http.get('http://bingbong.website/api/switches/list');
      },
      save: function(data){
        return $http.get('http://bingbong.website/api/switches/save', {params: data});
      }
    }
})

.controller('HomeCtrl', function($scope, $ionicActionSheet, $timeout, $state, httpService, $ionicPopup){

    $scope.switches = [
      {
        text: 'Light_1',
        checked: false
      }, 
      {
        text: 'Light_2',
        checked: true
      },
      {
        text: 'Fan', 
        checked: true
      }
    ]

    // Refresing data
    $scope.refreshSwitches = function(){
       //delete $http.defaults.headers.common['X-Requested-With'];
       httpService.list().then(
          function(result){
             $scope.switches = result.data;
             console.log(result.data);
          },
          function(err){
            var alertPopup = $ionicPopup.alert({
                title: 'Refresh failed!',
                template: 'Please check your Internet Connection'
            });
          }
       )
    }

    // Calling manually to update buttons.
    $scope.refreshSwitches();

    // watching value change
    $scope.updateSwitches = function(model){
        //var data = JSON.stringify({text:model.text, checked: model.checked});
        var data = {text:model.text, checked: model.checked};
        console.log(data);

        httpService.save(data).then(
          function(result){
             //$scope.switches = result.data;
             console.log(result.data);
          },
          function(err){
            var alertPopup = $ionicPopup.alert({
                title: 'Update failed!',
                template: 'Please check your Internet Connection'
            });
          }
       )
    }

    // logout function
    $scope.logout = function(){
        $state.go('login');
    }

    // Triggered on a button click, or some other target
   $scope.actionSheet = function() {

     // Show the action sheet
     var hideSheet = $ionicActionSheet.show({
       buttons: [
         { text: 'Logout' },
       ],
       cancelText: 'Cancel',
       cancel: function() {
            $timeout(function() {
               hideSheet();
            }, 1000);
          },
       buttonClicked: function(index) {
         if(index==0){
            $scope.logout();
         }
         return true;
       }
     });

   };
  
})

// Login Controller
.controller('LoginCtrl', function($scope, LoginService, $state, $ionicPopup){
  $scope.data = {};
 
  $scope.login = function(){
      LoginService.loginUser($scope.data.username, $scope.data.password).success(function(data) {
            $state.go('home');
        }).error(function(data) {
            var alertPopup = $ionicPopup.alert({
                title: 'Login failed!',
                template: 'Please check your credentials!'
            });
        });
    }
})